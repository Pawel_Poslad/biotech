import re

letters = input("Wprowadź sekwencje nukleotydów: ")
if re.match("^[a,c,g,t,A,C,G,T]*$", letters):
    letters = letters.upper()
    print("Pobrano sekwencje: " + letters)

    i = 0
    a = 0
    b = 3
    while i < len(letters):
        sekwencja_a = letters[a:b]
        print(sekwencja_a)
        i = i + 3
        a = a + 3
        b = b + 3

    i = 0
    a = 0
    b = 3
    while i < len(letters):
        sekwencja_b = letters[a:b]
        print(sekwencja_b, end='-')
        i = i + 3
        a = a + 3
        b = b + 3
else:
    print("Błąd, dostępne są tylko znaki a,c,g,t,A,C,G,T")
