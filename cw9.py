#Pętla for

a = int(input("Podaj wartość minimalną: "))
b = int(input("Podaj wartość maksymalną: "))
a_b_list=[]

if a > b:
    print("Błąd warość minimalna nie może być większa od maksymalnej")
else:

    for i in range(a+1, b, +1):
        a_b_list.append(i)

    print("\nLiczby całkowite w zbiorze w od {0} do {1} porządku rosnącnym, przedstawione za pomocą pętli for to: ".format(a, b))

    for element in a_b_list:
        print(element, end = ' ')

    print("\nLiczby całkowite w zbiorze w od {0} do {1} porządku malejącym, przedstawione za pomocą pętli for to: ".format(a, b))

    a_b_list.reverse()

    for element in a_b_list:
        print(element, end = ' ')

#Pętla while

    print("\nLiczby całkowite w zbiorze w od {0} do {1} porządku rosnącnym, przedstawione za pomocą pętli while to: ".format(a, b))

    a_b_list.reverse()
    i = 0
    while i < len(a_b_list):
        print(a_b_list[i], end = ' ')
        i = i + 1


    print("\nLiczby całkowite w zbiorze w od {0} do {1} porządku malejącym, przedstawione za pomocą pętli while to: ".format(a, b))

    a_b_list.reverse()
    i = 0
    while i < len(a_b_list):
        print(a_b_list[i], end = ' ')
        i = i + 1
